import React from 'react';
import ReactDOM from 'react-dom';
import { Switch, Route, BrowserRouter } from 'react-router-dom';

import reducers from './reducers/root';
import { createStore } from 'redux';
import { Provider } from 'react-redux';

import './index.css';
import MainSearch from './containers/mainSearch/mainSearch';
import SearchResult from './containers/searchResult/searchResult';
import DetailedView from './containers/detailedView/detailedView';

const store = createStore(reducers);
ReactDOM.render(
    <Provider store={store}>
        <BrowserRouter>
            <Switch>
                <Route exact path="/" component={MainSearch} />
                <Route path="/search-result" component={SearchResult} />
                <Route exact path="/detail-view" component={DetailedView} />
            </Switch>
        </BrowserRouter>
    </Provider>,
    document.getElementById('root'),
);
