import { Map } from 'immutable';
import {
    mainSearchActions,
} from '../constants';

const initialState = Map({
    error: false,
    suggestions: null,
    currentSuggestions: '',
});

function mainSearchReducer(state = initialState, action) {
    switch (action.type) {
        case mainSearchActions.REQUEST_SUGGESTIONS:
            return state.set('error', false);

        case mainSearchActions.SUCCESS_SUGGESTIONS: {
            return state.set('error', false).set('suggestions', action.payload);
        }
        case mainSearchActions.RESET_SUGGESTIONS:
            return initialState;

        case mainSearchActions.FAILURE_SUGGESTIONS:
            return state.set('error', action.payload);
        case mainSearchActions.SELECT_SUGGESTIONS:
            return state.set('currentSuggestions', action.payload);
        default:
            return state;
    }
}

export function getCurrentSuggestions(state) {
    if (state.mainSearchReducer.get('currentSuggestions') == null) {
        return null;
    } else {
        return state.mainSearchReducer.get('currentSuggestions');
    }
}

export function getSuggestions(state) {
    if (state.mainSearchReducer.get('suggestions') == null) {
        return null;
    } else {
        return state.mainSearchReducer.get('suggestions');
    }
}

export default mainSearchReducer;
