import { Map } from 'immutable';
import { searchResultActions } from '../constants';

const initialState = Map({
    error: false,
    currentBook: '',
});

function searchResultReducer(state = initialState, action) {
    switch (action.type) {
        case searchResultActions.SELECT_BOOK:
            return state.set('currentBook', action.payload);
        default:
            return state;
    }
}

export function getCurrentBook(state) {
    if (state.searchResultReducer.get('currentBook') == null) {
        return null;
    } else {
        return state.searchResultReducer.get('currentBook');
    }
}

export default searchResultReducer;
