// Redux
import { combineReducers } from 'redux';

import mainSearchReducer from './mainSearch';
import searchResultReducer from './searchResult';

export default combineReducers({
    mainSearchReducer,
    searchResultReducer,
    // result: ,
    // view: ,
});
