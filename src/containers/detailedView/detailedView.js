import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import ReactStars from 'react-stars';

import IconLastArrow from '../../icons/iconLastArrow';

import { getCurrentBook } from '../../reducers/searchResult';
import { fetchCurrentBook } from '../../actions/searchResult';

import './styles.css';

class DetailedView extends Component {
    constructor(props) {
        super(props);
        this.state = { infoBook: null };
    }
    componentDidMount() {
        this.requestListBookById();
    }

    requestListBookById = () => {
        const { currentBook } = this.props;
        return fetch(`https://www.googleapis.com/books/v1/volumes/${currentBook}`)
            .then(r => r.json())
            .then(res => {
                this.setState({
                    infoBook: res,
                });
            });
    };

    render() {
        const { infoBook } = this.state;
        const { currentBook } = this.props;
        return currentBook && infoBook ? (
            <div className="view-root">
                <div className="view-container">
                    <div className="view-container-title">
                        <div className="view-button">
                            <Link to="/search-result">
                                <IconLastArrow className="view-button" width="25" height="25" />
                            </Link>
                        </div>
                        <div className="view-title">
                            <h3>{infoBook.volumeInfo.title}</h3>
                        </div>
                    </div>

                    <div className="view-avatar-div">
                        {infoBook.volumeInfo.imageLinks &&
                        infoBook.volumeInfo.imageLinks.smallThumbnail ? (
                            <img
                                key={infoBook.id}
                                src={infoBook.volumeInfo.imageLinks.large}
                                className="view-avatar"
                                alt=""
                            />
                        ) : null}
                    </div>

                    <div className="view-text">
                        <div className="view-stars">
                            <ReactStars
                                count={5}
                                size={24}
                                value={
                                    infoBook.volumeInfo.averageRating
                                        ? infoBook.volumeInfo.averageRating
                                        : 0
                                }
                                color2={'#ffd700'}
                            />
                        </div>
                        <div className="view-authors">
                            <b>Авторы: </b>
                            {infoBook.volumeInfo.authors.map(author => (
                                <div className="view-authors">{author}</div>
                            ))}
                        </div>
                        {infoBook.volumeInfo.categories ? (
                            <div className="view-categories">
                                <b>Категория: </b>
                                {infoBook.volumeInfo.categories.map(cat => (
                                    <div className="view-categories">{cat}</div>
                                ))}
                            </div>
                        ) : null}
                        <div className="view-publication-date">
                            <b>Дата публикации: </b>
                            {infoBook.volumeInfo.publishedDate}
                        </div>
                    </div>

                    {infoBook.volumeInfo.description ? (
                        <div className="view-description">
                            <b>Описание: </b>
                            <div
                                className="view-description"
                                dangerouslySetInnerHTML={{
                                    __html: infoBook.volumeInfo.description,
                                }}
                            />
                        </div>
                    ) : null}
                </div>
            </div>
        ) : (
            <div className="view-title">
                <Link to="/searchResult"> Назад </Link>
                <h1>Отсутсвуют данные для отображения</h1>
            </div>
        );
    }
}
function mapStateToProps(state) {
    return {
        currentBook: getCurrentBook(state),
    };
}
const mapDispatchToProps = dispatch => {
    return {
        fetchCurrentBook: bindActionCreators(fetchCurrentBook, dispatch),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(DetailedView);
