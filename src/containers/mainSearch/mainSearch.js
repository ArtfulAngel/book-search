import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';

import { bindActionCreators } from 'redux';
import { Link } from 'react-router-dom';
import Parser from 'xml2js-parser';

import IconSearch from '../../icons/iconSearch';

import { getCurrentSuggestions, getSuggestions } from '../../reducers/mainSearch';
import {
    requestSuggestions,
    successSuggestions,
    failureSuggestions,
    fetchCurrentSuggestions,
} from '../../actions/mainSearch';

import './styles.css';

class MainSearch extends Component {
    static defaultProps = {
        suggestions: [],
    };

    state = {
        itemContainerBox: {},
        suggestions: [],
        inputValue: '',
        opened: false,
    };

    componentDidMount() {
        window.addEventListener('scroll', this.setItemsContainerBox);
    }

    componentWillMount() {
        window.removeEventListener('scroll', this.setItemsContainerBox);
    }

    /* получаем размер и местоположение контэйнера
     * и возвращаем объект с этими данными
     */
    getItemsContainerBox = () => {
        if (!this.conteinerElement || !this.itemsContainerElement) {
            return;
        }
        const conteinerElementBoundery = this.conteinerElement.getBoundingClientRect();
        //        const itemsContainerElementBoundery = this.itemsContainerElement.getBoundingClientRect();
        const left = conteinerElementBoundery.left;
        const top = conteinerElementBoundery.bottom;
        return {
            width: `${this.conteinerElement.offsetWidth}px`,
            left: `${left}px`,
            top: `${top}px`,
        };
    };

    /**
     *  устанавливаем в state значение размеров
     * для контейнера с items
     */
    setItemsContainerBox = () => {
        const itemContainerBox = this.getItemsContainerBox();
        this.setState({ itemContainerBox });
    };

    /**
     * метод для фиксации открытия списка
     * и размеров
     */
    openItemsContainer = () => {
        this.setState(
            state => ({
                opened: true,
            }),
            this.setItemsContainerBox,
        );
    };

    /**
     * метод закрытия контейнера
     *
     */
    closeItemsContainer = () => {
        this.setState({ opened: false });
    };

    /**
     * событие, срабатывающее при выборе текста из подсказок
     */
    onItemChose = event => {
        const { fetchCurrentSuggestions } = this.props;
        fetchCurrentSuggestions(event.target.innerText);
        this.setState({ inputValue: event.target.innerText });
    };

    /**
     * метод для отрисовки автокомлита
     */
    renderItemsContainer = () => {
        const { suggestions } = this.props;
        var parser = new Parser({ trim: true });
        parser.parseString(suggestions, (err, result) => {
            let newSuggestions = [];
            result.toplevel.CompleteSuggestion.map(s => {
                newSuggestions.push(s.suggestion[0].$.data);
            });
            this.setState({ suggestions: newSuggestions });
        });
        return (
            <div className="main-items-container-wrapper" onClick={this.closeItemsContainer}>
                <div
                    className="main-items-container"
                    ref={itemsContainer => {
                        this.itemsContainerElement = itemsContainer;
                    }}
                    style={this.state.itemContainerBox}
                >
                    {this.state.suggestions ? (
                        <ul className="main-items-list">
                            {this.state.suggestions.map((item, index) => (
                                <li
                                    key={index}
                                    className="main-list-item"
                                    onClick={this.onItemChose}
                                >
                                    {item}
                                </li>
                            ))}
                        </ul>
                    ) : null}
                </div>
            </div>
        );
    };

    /**
     * метод для сохранения полученных подсказок
     * и их отображения
     */
    setSuggestions = event => {
        const inputValue = event.target.value || '';
        this.setState({ inputValue });
        const { requestSuggestions, successSuggestions, failureSuggestions } = this.props;
        if (inputValue) {
            requestSuggestions();
            return axios({
                url: `https://cors-anywhere.herokuapp.com/http://suggestqueries.google.com/complete/search?client=toolbar&ds=bo&q=${inputValue}`,
                timeout: 10000,
                method: 'get',
            })
                .then(response => {
                    successSuggestions(response.data);
                    this.openItemsContainer();
                })
                .catch(error => {
                    failureSuggestions(error);
                });
        } else {
            this.closeItemsContainer();
        }
    };

    render() {
        const { opened, inputValue } = this.state;

        return (
            <Fragment>
                <div className="main-root">
                    <div
                        className="main-container"
                        ref={root => {
                            this.conteinerElement = root;
                        }}
                    >
                        <div className="main-value">
                            <input
                                className="main-value-box"
                                value={inputValue}
                                onChange={this.setSuggestions}
                            />
                        </div>
                        <Link to="/search-result">
                            <IconSearch className="main-arrow-button" width="30" />
                        </Link>
                    </div>
                    {opened && this.renderItemsContainer()}
                </div>
            </Fragment>
        );
    }
}

function mapStateToProps(state) {
    return {
        suggestions: getSuggestions(state),
        currentSuggestions: getCurrentSuggestions(state),
    };
}

const mapDispatchToProps = dispatch => {
    return {
        requestSuggestions: bindActionCreators(requestSuggestions, dispatch),
        successSuggestions: bindActionCreators(successSuggestions, dispatch),
        failureSuggestions: bindActionCreators(failureSuggestions, dispatch),
        fetchCurrentSuggestions: bindActionCreators(fetchCurrentSuggestions, dispatch),
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(MainSearch);
