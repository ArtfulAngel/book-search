import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import ReactStars from 'react-stars';

import Pagination from '../pagination/pagination';
import { getCurrentSuggestions } from '../../reducers/mainSearch';
import { fetchCurrentBook } from '../../actions/searchResult';

import './styles.css';

class SearchResult extends Component {
    constructor(props) {
        super(props);
        this.state = {
            itemsBook: [],
            page: 1,
            countCurItems: 10,
            startIndex: 0,
            totalItems: null,
            totalPages: 1,
        };
    }

    componentDidMount() {
        this.fetchListBookByPages(null);
    }

    /**
     * запрос на получение списка книг по определенной странице
     */
    fetchListBookByPages = curPage => {
        const { currentSuggestions } = this.props;
        const { page, countCurItems, startIndex } = this.state;
        let nextStartIndex;
        if (curPage) {
            if (page !== curPage) {
                if (page < curPage) {
                    nextStartIndex = startIndex + countCurItems * (curPage - page);
                } else if (page > curPage) {
                    nextStartIndex = startIndex - countCurItems * (page - curPage);
                }

                this.setState({
                    page: curPage,
                    startIndex: nextStartIndex,
                });
            }
        } else nextStartIndex = startIndex;
        return fetch(`https://www.googleapis.com/books/v1/volumes?q=${currentSuggestions}&maxResults=${countCurItems}&startIndex=${nextStartIndex}&fields=items(id,selfLink,volumeInfo(description,imageLinks(smallThumbnail,thumbnail),title,averageRating)),kind,totalItems&key=AIzaSyDfCdIXRrk_NhrqOcuF8fGFfyU9mqEwy1Y
        `)
            .then(r => r.json())
            .then(res => {
                this.setState({
                    itemsBook: res.items,
                    totalItems: res.totalItems,
                    totalPages: Math.floor(res.totalItems / countCurItems),
                });
            });
    };

    /**
     * событие смены страницы
     */
    handlerChangePagination = event => {
        this.fetchListBookByPages(event.currentTarget.id);
    };

    /**
     * событие для кнопки "Подробнее"
     */
    handlerClickButton = event => {
        const { fetchCurrentBook } = this.props;
        fetchCurrentBook(event.currentTarget.id);
    };

    render() {
        const metadata = {
            page: this.state.page,
            totalPages: this.state.totalPages,
        };
        const { currentSuggestions } = this.props;
        return (
            <div className="search-result-root">
                {currentSuggestions ? (
                    <div className="search-result-container">
                        {this.state.itemsBook.map(book => (
                            <div key={book.id} className="search-result-row">
                                <div className="search-result-avatar-div">
                                    {book.volumeInfo.imageLinks &&
                                    book.volumeInfo.imageLinks.smallThumbnail ? (
                                        <img
                                            className="search-result-avatar"
                                            key={book.id}
                                            src={book.volumeInfo.imageLinks.smallThumbnail}
                                            alt=""
                                        />
                                    ) : null}
                                </div>
                                <div className="search-result-txt-conteiner">
                                    <h3>{book.volumeInfo.title}</h3>
                                    {book.volumeInfo.description ? (
                                        <p>{`${book.volumeInfo.description.slice(0, 200)} ...`}</p>
                                    ) : null}

                                    <div className="search-result-action-button">
                                        <ReactStars
                                            className="search-result-stars"
                                            count={5}
                                            size={24}
                                            value={
                                                book.volumeInfo.averageRating
                                                    ? book.volumeInfo.averageRating
                                                    : 0
                                            }
                                            color2={'#ffd700'}
                                        />
                                        <button
                                            id={book.id}
                                            className="search-result-buttons"
                                            onClick={this.handlerClickButton}
                                        >
                                            <Link to={`/detail-view`}>Подробнее</Link>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        ))}
                        <Pagination pagination={metadata} onClick={this.handlerChangePagination} />
                    </div>
                ) : (
                    <div>
                        <Link to="/"> Назад </Link>
                        <h1>Нет данных для поиска</h1>
                    </div>
                )}
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        currentSuggestions: getCurrentSuggestions(state),
    };
}
const mapDispatchToProps = dispatch => {
    return {
        fetchCurrentBook: bindActionCreators(fetchCurrentBook, dispatch),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(SearchResult);
