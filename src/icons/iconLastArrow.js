import React, { Component } from 'react';

class IconLastArrow extends Component {
    render() {
        const { width = 15, height = 15, color = '#8392a5', className } = this.props;
        return (
            <svg
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 90 90"
                width={`${width}px`}
                height={`${height}px`}
                className={className}
            >
                <path
                    d="M90 79S80 30.333 36.125 30.333V11L0 43.276l36.125 33.455V54.94C59.939 54.94 77.582 57.051 90 79z"
                    fill="#030104"
                />
            </svg>
        );
    }
}

export default IconLastArrow;
