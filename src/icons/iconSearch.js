import React, { Component } from 'react';

class IconSearch extends Component {
  render() {
    const {
      width = 15,
      height = 15,
      color = '#8392a5',
      className
    } = this.props;
    return (
      <svg
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 16 16"
        width={`${width}px`}
        height={`${height}px`}
        className={className}
      >
        <path
          fill={color}
          d="M14.844 14.324l-3.118-3.252c.877-1.165 1.405-2.63 1.405-4.222 0-3.78-2.946-6.854-6.568-6.854-3.623 0-6.57 3.075-6.57 6.854 0 3.78 3.118 6.617 6.74 6.617 1.42 0 2.56-.24 3.637-1.042l3.146 3.284c.184.19.424.286.665.286.24 0 .48-.096.664-.287.366-.383.366-1.003 0-1.386zM1.868 6.85c0-2.7 2.106-4.895 4.694-4.895 2.587 0 4.692 2.196 4.692 4.895 0 2.7-1.934 4.934-4.52 4.934-2.59 0-4.866-2.234-4.866-4.934z"
        />
      </svg>
    );
  }
}

export default IconSearch;
