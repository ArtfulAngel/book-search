import { searchResultActions } from '../constants';

export function fetchCurrentBook(selectBook) {
    return {
        type: searchResultActions.SELECT_BOOK,
        payload: selectBook,
    };
}
