import {
    mainSearchActions
} from '../constants';

export function requestSuggestions() {
    return { type:  mainSearchActions.REQUEST_SUGGESTIONS };
}
export function successSuggestions(suggestions) {
    return {
        type:  mainSearchActions.SUCCESS_SUGGESTIONS,
        payload: suggestions,
    };
}
export function failureSuggestions(error) {
    return {
        type:  mainSearchActions.FAILURE_SUGGESTIONS,
        payload: error,
    };
}
export function fetchCurrentSuggestions(selectText) {
    return {
        type:  mainSearchActions.SELECT_SUGGESTIONS,
        payload: selectText,
    };
}
